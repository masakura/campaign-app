﻿using Campaign.WebApp.Models.Customers;

namespace Campaign.WebApp.Models.Campaigns
{
    public interface IApplyCampaignService
    {
        /// <summary>
        ///     キャンペーを申し込みます。
        /// </summary>
        /// <param name="customer">申込者。</param>
        /// <returns>受付番号。</returns>
        int Apply(Customer customer);
    }
}