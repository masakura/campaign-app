﻿using Campaign.WebApp.Models.Customers;

namespace Campaign.WebApp.Models.Campaigns
{
    internal sealed class ApplyCampaignService : IApplyCampaignService
    {
        private readonly AcceptNumberGenerator _acceptNumberGenerator;
        private readonly ICampaignApplicationRepository _applications;

        public ApplyCampaignService(AcceptNumberGenerator acceptNumberGenerator,
            ICampaignApplicationRepository applications)
        {
            _acceptNumberGenerator = acceptNumberGenerator;
            _applications = applications;
        }

        public int Apply(Customer customer)
        {
            var acceptNumber = _acceptNumberGenerator.NewNumber();

            var application = new CampaignApplication(acceptNumber, customer.Id);

            _applications.Add(application);

            return acceptNumber;
        }
    }
}