﻿using System;

namespace Campaign.WebApp.Models.Campaigns
{
    public sealed class CampaignApplication
    {
        public CampaignApplication(int acceptNumber, Guid customerId)
        {
            AcceptNumber = acceptNumber;
            CustomerId = customerId;
        }

        // ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
        public int AcceptNumber { get; private set; }
        public Guid CustomerId { get; private set; }
    }
}