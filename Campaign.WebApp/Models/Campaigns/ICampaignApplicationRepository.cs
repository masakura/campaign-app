﻿namespace Campaign.WebApp.Models.Campaigns
{
    public interface ICampaignApplicationRepository
    {
        void Add(CampaignApplication application);
    }
}