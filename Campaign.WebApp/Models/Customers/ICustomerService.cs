﻿using System;
using System.Collections.Generic;

namespace Campaign.WebApp.Models.Customers
{
    public interface ICustomerService
    {
        IEnumerable<Customer> All();
        Customer Find(Guid customerId);
    }
}