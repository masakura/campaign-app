﻿using System;
using System.Diagnostics;
using Campaign.WebApp.Models;
using Campaign.WebApp.Models.Campaigns;
using Campaign.WebApp.Models.Customers;
using Microsoft.AspNetCore.Mvc;

namespace Campaign.WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICustomerService _customers;
        private readonly IApplyCampaignService _campaignService;

        public HomeController(ICustomerService customers, IApplyCampaignService campaignService)
        {
            _customers = customers;
            _campaignService = campaignService;
        }

        public IActionResult Index()
        {
            var customers = _customers.All();

            return View(customers);
        }

        [HttpPost]
        public IActionResult Apply(Guid customerId)
        {
            var customer = _customers.Find(customerId);

            var acceptNumber = _campaignService.Apply(customer);

            return View(acceptNumber);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}