﻿using Campaign.WebApp.Models.Campaigns;

namespace Campaign.WebApp.Infrastructure
{
    internal sealed class CampaignApplicationRepository : ICampaignApplicationRepository
    {
        private readonly ApplicationDbContext _db;

        public CampaignApplicationRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public void Add(CampaignApplication application)
        {
            _db.CampaignApplications.Add(application);
            _db.SaveChanges();
        }
    }
}